//
//  LondonPlace.swift
//  LondonLandmarks
//
//  Created by Moises Gil on 4/17/18.
//  Copyright © 2018 Moises Gil. All rights reserved.
//

import UIKit

class LondonPlace: NSObject {
    
    var placeTitle = ""
    var placeDescription = ""
    var placeAddress = ""
    var latitude = 0.0
    var longitude = 0.0
    var placeImage = UIImage()

}
