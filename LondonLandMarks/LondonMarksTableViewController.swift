//
//  LondonMarksTableViewController.swift
//  LondonLandmarks
//
//  Created by Moises Gil on 4/17/18.
//  Copyright © 2018 Moises Gil. All rights reserved.
//

import UIKit

class LondonMarksTableViewController: UITableViewController {
    
    var placeData = [LondonPlace]()
    let numberOfSection = 1
    let cellIdentifier = "cell"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        let dataSource = PlacesDatasource()
        
        placeData = dataSource.getData()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSection
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeData.count
    }


    //Function that create the table cell to show
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TableCell

        cell.placeTitle.text = placeData[indexPath.row].placeTitle
        cell.placeDescription.text = placeData[indexPath.row].placeAddress
        cell.placeImage.image = placeData[indexPath.row].placeImage
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "showDetail" {
            
            
            let detailView = segue.destination as! DetailViewController
            
            
            if let selectedRowIndex = tableView.indexPathForSelectedRow {
                
                detailView.placeToDetail = placeData[selectedRowIndex.row]
                
            }
            
        }
        
    }
    
}
