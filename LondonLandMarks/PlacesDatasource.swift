//
//  PlacesDatasource.swift
//  LondonLandmarks
//
//  Created by Moises Gil on 4/17/18.
//  Copyright © 2018 Moises Gil. All rights reserved.
//

import UIKit

class PlacesDatasource {
    
    //Properties of the class
    
    var imageList = ["BigBen.jpg","BuckinghamPalace.jpg","LondonEye.jpg","St-Pauls.jpg","TowerBridge.jpg","WestminsterAbbey.jpg"]
    
    var titleList = ["Big Ben","Buckingham Palace","London Eye","St Paul's Cathedral","Tower Bridge","Westminster Abbey"]
    
    var descriptionList = ["""
                            Big Ben is the nickname for the Great Bell of the clock at the north end of the Palace of Westminster in London[1] and is usually extended to refer to both the clock and the clock tower.[2][3] The official name of the tower in which Big Ben is located was originally the Clock Tower, but it was renamed Elizabeth Tower in 2012 to mark the Diamond Jubilee of Elizabeth II.
                           """,
                           
                           """
                            Buckingham Palace (UK: /ˈbʌkɪŋəm ˈpælɪs/[1][2]) is the London residence and administrative headquarters of the monarch of the United Kingdom.[a][3] Located in the City of Westminster, the palace is often at the centre of state occasions and royal hospitality. It has been a focal point for the British people at times of national rejoicing and mourning.
                           """
                           ,
                           """
                            The London Eye, known for sponsorship reasons as the Coca-Cola London Eye, is a giant Ferris wheel on the South Bank of the River Thames in London. The structure is 443 feet (135 m) tall and the wheel has a diameter of 394 feet (120 m). When it opened to the public in 2000 it was the world's tallest Ferris wheel.
                           
                           """,
                           """
                            St Paul's Cathedral, London, is an Anglican cathedral, the seat of the Bishop of London and the mother church of the Diocese of London. It sits on Ludgate Hill at the highest point of the City of London and is a Grade I listed building. Its dedication to Paul the Apostle dates back to the original church on this site, founded in AD 604.

                           """,
                           """
                            Tower Bridge is a combined bascule and suspension bridge in London built between 1886 and 1894. The bridge crosses the River Thames close to the Tower of London and has become an iconic symbol of London. Because of this, Tower Bridge is sometimes confused with London Bridge, situated some 0.5 mi (0.80 km) upstream.
                           """,
                           """
                            Westminster Abbey, formally titled the Collegiate Church of St Peter at Westminster, is a large, mainly Gothic abbey church in the City of Westminster, London, England, just to the west of the Palace of Westminster. It is one of the United Kingdom's most notable religious buildings and the traditional place of coronation and burial site for English and, later, British monarchs.
                            """
    ]
    
    let placeAddresses = ["London SW1A 0AA","London SW1A 1AA","London SE1 7PB","London EC4M 8AD","London SE1 2UP","London SW1P 3PA"]
    

    let latitudes = [51.50072919999999,51.501364,51.503324,51.5138453,51.5043192,51.4994174]
    let longitudes = [-0.12462540000001354,-0.1418899999999894,-0.1195430000000215,-0.0983506000000034,
                      -0.07616669999993064,-0.1275705000000471]

    
    
    var  data = [LondonPlace]()
    
    
    func getData() -> [LondonPlace] {
        
        if data.count == 0 {
            loadData()
        }
        
        return  data
        
    }
    
    func loadData() {
        
        for(index, image) in imageList.enumerated() {
            
            let place = LondonPlace()
            
            place.placeTitle = titleList[index]
            place.placeDescription = descriptionList[index]
            place.placeAddress = placeAddresses[index]
            place.placeImage = UIImage(named: image)!
            place.latitude = latitudes[index]
            place.longitude = longitudes[index]
            
            data.append(place)
            
        }
        
    }
}
