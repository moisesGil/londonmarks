//
//  DetailViewController.swift
//  LondonLandmarks
//
//  Created by Moises Gil on 4/18/18.
//  Copyright © 2018 Moises Gil. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeDescription: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeMapView: MKMapView!
    @IBOutlet weak var directionButton: UIButton!
    
    var placeToDetail = LondonPlace()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //placeTitle.textColor = UIColor(red: 35/255, green: 90/255, blue: 141/255, alpha: 1.0)
        
        directionButton.backgroundColor =  UIColor(red: 35/255, green: 90/255, blue: 141/255, alpha: 1.0)
        directionButton.layer.cornerRadius = 2
        
        placeMapView.layer.cornerRadius = 2
        
        navigationItem.title = placeToDetail.placeTitle
        
        placeTitle.text = placeToDetail.placeTitle
        placeDescription.text = placeToDetail.placeAddress
        textView.text = placeToDetail.placeDescription
        placeImageView.image = placeToDetail.placeImage
        
        
        //Indicate the zoom level of the map
        let mapSpan: MKCoordinateSpan = MKCoordinateSpanMake(0.004, 0.004)
        
        //Location of the place in the map
        let placeLocationCoordinate = CLLocationCoordinate2DMake(placeToDetail.latitude, placeToDetail.longitude)
        
        //Region
        let mapRegion = MKCoordinateRegion(center: placeLocationCoordinate, span: mapSpan)
        
        //Setting the region to the map
        placeMapView.setRegion(mapRegion, animated: true)
        
        
        //Creating visual pin for the map
        
        let mapPin = MKPointAnnotation()
        
        mapPin.coordinate = placeLocationCoordinate
        mapPin.title = placeToDetail.placeTitle
        mapPin.subtitle = placeToDetail.placeAddress
        
        placeMapView.addAnnotation(mapPin)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getDirections(_ sender: UIButton) {
        
        UIApplication.shared.openURL(URL(string: "https://maps.apple.com?daddr=\(placeToDetail.latitude),\(placeToDetail.longitude)")!)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
