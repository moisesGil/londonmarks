//
//  PlacesDataSource.swift
//  LondonLandmarks
//
//  Created by Moises Gil on 4/17/18.
//  Copyright © 2018 Moises Gil. All rights reserved.
//

import UIKit

protocol DataSource {
    
    associatedtype EntityType
    
    func getData() -> [EntityType]
    
}
